
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author vinicius
 */
public class Pratica31 {

    private static String meuNome = "Vinicius Pinheiro".toUpperCase();
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1997, Calendar.DECEMBER, 26);
    private static GregorianCalendar dataAtual = new GregorianCalendar();
    private static long dif = dataAtual.getTime().getTime() - dataNascimento.getTime().getTime();
    private static long dias = dif / 86400000;
    private static Date inicio,fim;

    public static void main(String[] args) {

        inicio = new Date();
        System.out.println(meuNome);
        System.out.println(meuNome.charAt(9)+meuNome.substring(10, 17).toLowerCase()+ ", " + meuNome.charAt(0)+".");
        System.out.println(dias);
        fim = new Date();
        System.out.println(fim.getTime()-inicio.getTime());
    }

}